/*jshint devel:true */
/*global Backbone */
/*global _ */

$((function () {

  'use strict';

  //Define Model
  var Book = Backbone.Model.extend({
    defaults: function () {
      return {
        'idProduct': 0,
        'nameProduct': ''
      };
    }
  });

  // var hobbit = new Book({'isbn': 123, 'title': 'El Hobbit'});
  // console.log(hobbit);
  // console.log(hobbit.get('title'));

  //Collection - Ajax
  var Library = Backbone.Collection.extend({
    model: Book,

    url: 'http://www.tuttifrescuni.com/silex-api/www/index.php/seasons/best',

    comparator: 'nameProduct',

    getProducts: function(options) {

      var self = this;

      this.fetch({
        reset: true,
        success: function (collection) {
            self.trigger('successOnFetch', collection, options);
        },
        error: function () {
            self.trigger('errorOnFetch');
        }
      });
    }

  });

  //Item View
  var ItemView = Backbone.View.extend({

    tagName:  'div',

    className: 'col-xs-6 col-md-3',

    template: _.template($('#item-template').html()),

    render: function() {

      this.$el.html(this.template(this.model.toJSON()));

      return this;
    }

  });

  //List View
  var ListView = Backbone.View.extend({

    tagName: 'div',
    className: 'row',

    initialize: function () {

        this.listenTo(this.collection, 'successOnFetch', this.handleSuccess);
        this.listenTo(this.collection, 'errorOnFetch', this.handleError);
    },

    handleSuccess: function (collection, options) {

        this.collection.each(function (item) {
          this.$el.append(new ItemView({model: item}).render().el);
        }, this);

        if (options.query) {

          this.moreInfo(options.query);

        } else {

          $('#products').html(this.render().el);

        }
    },

    handleError: function () {
        console.log('Error');
    },

    moreInfo: function(query) {

      this.collection.each(function (model) {

        if (model.get('nameProduct') === query) {

          console.log(model.get('SmallPhoto'));
        }

      }, this);
    }
  });

  // App
  var App = function() {

    var products;

    var collection;

    var AppRouter = Backbone.Router.extend({

      routes: {
        '':                 'home',
        'products/:query':  'product'  // #product/kiwis
      },


      home: function() {

        var options = {'type': 'home'};

        this.createProducts(options);

      },

      product: function(query) {

        if (products) {

          products.moreInfo(query);

        } else {

          var options = {'type': 'home', 'query': query};

          this.createProducts(options);

        }

      },

      createProducts: function(options) {

        collection = new Library();

        products = new ListView({'collection': collection});

        collection.getProducts(options);
      },

    });

    var init = function() {

        new AppRouter();

        Backbone.history.start();

    };

    return {
        init: init
    };
  };

  var app = new App();

  app.init();

})());
