# TALLER FRONT-END

[![Backbone](http://backboneconf.com/img/backbone-logo.png)](http://backbonejs.org)
[![Yeoman](http://bruno.garciaechegaray.com/TallerFrontEnd/tool-yo.png)](http://yeoman.io)
[![Bower](http://bruno.garciaechegaray.com/TallerFrontEnd/tool-bower.png)](http://bower.io)
[![Grunt](http://bruno.garciaechegaray.com/TallerFrontEnd/tool-grunt.png)](http://gruntjs.com)

***

## INTRO

* [Documentacion](http://goo.gl/Ot7HnN)

***

## TOOLS

### Node

[https://nodejs.org/download/](https://nodejs.org/download/)

### Yeoman

	$ sudo npm install -g yo

### Bower

	$ sudo npm install -g bower
	$ sudo npm update -g bower

### Grunt

	$ sudo npm install -g grunt-cli

***

## YEOMAN

	$ sudo npm install -g generator-webapp
	$ yo webapp

	$ bower install 
	$ npm install
	$ grunt serve

***

## BOWER

### Add dependencies

	$ bower search --h
	
	$ bower install backbone --save
	$ bower install underscore --save
	$ bower install jquery --save

### Alternatives

	jquery -> zepto
	underscore -> lodash

### The semver parser for node

	https://github.com/npm/node-semver

***

## GRUNT

### Install copy plugin

	$ npm install grunt-contrib-copy --save-dev
	$ grunt copy

### Install connect plugin

	$ npm install grunt-contrib-connect --save-dev	

***

## APP

###	Check versions

	$(function () {

		"use strict"

		console.log($.fn.jquery);	
		console.log(_.VERSION);	
		console.log(Backbone.VERSION);

	}());

### Backbone Model

[https://bitbucket.org/snippets/bruno_poun/LMrk](https://bitbucket.org/snippets/bruno_poun/LMrk)


### Backbone Collection

Simple

[https://bitbucket.org/snippets/bruno_poun/jELb](https://bitbucket.org/snippets/bruno_poun/jELb)	

Ajax

[https://bitbucket.org/snippets/bruno_poun/oqnX](https://bitbucket.org/snippets/bruno_poun/oqnX)

### Backbone - View

[https://bitbucket.org/snippets/pounstudio/eELG](https://bitbucket.org/snippets/pounstudio/eELG)

### Backbone - Events

[https://bitbucket.org/snippets/bruno_poun/LMrK](https://bitbucket.org/snippets/bruno_poun/LMrK)

### Backbone - Router